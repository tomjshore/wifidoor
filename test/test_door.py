import sys
from unittest import TestCase
from unittest.mock import patch, MagicMock, ANY
import fake_rpi

fake_rpi.RPi.GPIO = MagicMock()


sys.modules["RPi.GPIO"] = fake_rpi.RPi.GPIO
from wifidoor import door


class TestDoor(TestCase):

    def test_when_door_open_call_function(self):
        port_id = 4
        callback = lambda _: print("")
        door.pi_listen_for_open_door(port_id, callback)

        fake_rpi.RPi.GPIO.add_event_detect.assert_called_with(
            4, fake_rpi.RPi.GPIO.FALLING, callback=callback, bouncetime=ANY
        )
