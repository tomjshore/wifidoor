from wifidoor.door import listen_for_open_door


if __name__ == "__main__":
    listen_for_open_door(lambda _: print("Door is open"))
