from typing import Callable
import RPi.GPIO as GPIO


__DEFAULT_PIN__ = 14


def listen_for_open_door(callback: Callable):
    pi_listen_for_open_door(__DEFAULT_PIN__, callback)


def pi_listen_for_open_door(pin_id: int, callback: Callable):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin_id, GPIO.IN)
    GPIO.add_event_detect(pin_id, GPIO.FALLING, callback=callback, bouncetime=100)
